<?php

use App\Http\Controllers\AdminpanelController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserviewController;
use Illuminate\Support\Facades\Route;

Route::get('/', [UserviewController::class,'index']);

Route::get('/home', [UserviewController::class,'home']);
Route::get('/all-contents', [UserviewController::class,'allContents']);
Route::get('/content-documents/{content}', [UserviewController::class,'contentDocuments']);


Route::get('/documents-lists', [UserviewController::class,'documentsLists']);
Route::get('/trainings-lists', [UserviewController::class,'trainingsLists']);
Route::get('/trainings-details/{training}', [UserviewController::class,'trainingsDetails']);

// Route::get('/details', function () {
//     $directory = __DIR__ .'/../public/storage/media';

//     $files = File::directories($directory);
//     dd($files);
//     return view('userview.details', [
//         'files' => $files,
//     ]);
// });

Route::prefix('admin')->group(function () {
    Route::get('/dashboard', [AdminpanelController::class,'dashboard']);
    Route::resource('/contents', ContentController::class);
    Route::resource('/trainings', TrainingController::class);
    Route::get('/courses/create/sub-folder', [TrainingController::class,'createSubFolder']);
    Route::get('/train/choosetitle', [TrainingController::class,'choosetitle'])->name('choosetitle');
    Route::resource('/users', UserController::class);
    Route::resource('/documents', DocumentController::class);
});


Route::get('/checkSlugDoc', [DocumentController::class,'checkSlugDoc'])->name('checkSlugDoc');
Route::get('/checkSlugTraining', [TrainingController::class,'checkSlugTraining'])->name('checkSlugTraining');
