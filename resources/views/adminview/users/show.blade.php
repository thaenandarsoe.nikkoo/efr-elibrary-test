@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Users" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <button class="btn btn-sm  shadow-none"><input type="checkbox" name="" id=""></button>
                        <button class="btn btn-sm btn-info shadow-none"> <i class="bi bi-check2-square"></i> 0
                            selected</button>
                        <button class="btn btn-sm btn-danger shadow-none"> <i class="bi bi-trash"></i> Bulk
                            Delete</button>

                        <div class="table-responsive">
                            <!-- Table with stripped rows -->
                            <table class="table datatable">
                                <thead>
                                    <tr>

                                        <th scope="col"> </th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Avatar</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>

                                        <th scope="row"> <input type="checkbox" name="" id=""> </th>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->role}}</td>
                                        <td><img src="{{$user->avatar}}" alt="Avatar" width="50px" height="50px"
                                                class="rounded-circle">
                                        </td>
                                        <td>
                                            <span class="btn btn-sm btn-success mb-3"><i
                                                    class="bi bi-pencil-square"></i>
                                            </span>
                                            <a href="#"><span class="btn btn-sm btn-warning mb-3"><i
                                                        class="bi bi-eye"></i></span></a>
                                            <span class="btn btn-sm btn-danger mb-3"><i class="bi bi-trash"></i></span>

                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>

                            </table>

                            <!-- End Table with stripped rows -->
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection