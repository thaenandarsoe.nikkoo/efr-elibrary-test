@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Register" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">


                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="#" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="name" id="name" required
                                        placeholder="Name">
                                    <label for="name">Name <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="username" id="username"
                                        readonly placeholder="Username">
                                    <label for="username">Username <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="email" class="form-control shadow-none" name="email" id="email"
                                        placeholder="Email" required>
                                    <label for="email">Email <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="phone" id="phone"
                                        placeholder="Contact Number" required>
                                    <label for="phone">Contact Number <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="password" class="form-control shadow-none" name="password"
                                        id="password" placeholder="Password" required>
                                    <label for="password">Password <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="file" class="form-control shadow-none" name="avatar" id="avatar"
                                        required placeholder="Avatar">
                                    <label for="avatar">Avatar<span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating mb-3">
                                    <select class="form-select shadow-none" name="role" id="role" aria-label="Role">
                                        <option selected> -- Select--</option>
                                        <option value="user">User</option>
                                        <option value="normal_admin">Normal Administrator</option>
                                        <option value="super_admin">Super Administrator</option>
                                    </select>
                                    <label for="role">Role <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-floating d-flex mt-1 align-items-center">
                                    <button type="submit" class="btn btn-primary btn-get-started">Register <i
                                            class="bi bi-ui-checks"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(document).ready(function(){
    $('#title').change(function(e){
        $.get('{{ route("checkSlugContent") }}',
            {'title':$(this).val(),},
            function (data){
                $('#slug').val(data.slug);
            }
        );
    });
    });

</script>
@endsection