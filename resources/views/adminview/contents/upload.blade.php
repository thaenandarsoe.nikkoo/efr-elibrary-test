@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Create Content" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">


                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="" method="post">
                            @csrf
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="name" id="name"
                                        placeholder="Name">
                                    <label for="name">Name<span>*</span> </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="slug" id="slug"
                                        placeholder="Slug">
                                    <label for="slug">Slug<span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating mb-3">
                                    <select class="form-select shadow-none" name="type" id="type" aria-label="Content">
                                        <option selected> -- Select--</option>

                                        <option value="trainings">Trainings</option>
                                        <option value="documents">Documents</option>


                                    </select>
                                    <label for="type">Content Type <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-floating d-flex justify-content-start align-items-center">
                                    <button type="submit" class="btn btn-primary btn-get-started">Create <i
                                            class="bi bi-upload"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection