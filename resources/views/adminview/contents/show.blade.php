@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Contents" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">
                        <button class="btn btn-sm  shadow-none"><input type="checkbox" name="" id=""></button>
                        <button class="btn btn-sm btn-info shadow-none"> <i class="bi bi-check2-square"></i> 0
                            selected</button>
                        <button class="btn btn-sm btn-danger shadow-none"> <i class="bi bi-trash"></i> Bulk
                            Delete</button>

                        <div class="table-responsive">
                            <!-- Table with stripped rows -->
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Slug</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Created by</th>
                                        <th scope="col">Date Create</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($contents as $content)
                                    <tr>
                                        <th scope="row">{{$sr_no++}}</th>
                                        <td>{{$content->name}}</td>
                                        <td>{{$content->slug}}</td>
                                        <td>{{$content->type}}</td>
                                        <th scope="row">{{$content->created_by}}</th>
                                        <td>{{$content->created_at->format('d-M-Y')}}</td>

                                        <td><span class="btn btn-sm btn-success mb-2"><i
                                                    class="bi bi-pencil-square"></i>
                                            </span>
                                            <span class="btn btn-sm btn-danger mb-2"><i class="bi bi-trash"></i></span>
                                            {{-- <span class="badge bg-warning text-dark">View</span> --}}
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                            <!-- End Table with stripped rows -->
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection