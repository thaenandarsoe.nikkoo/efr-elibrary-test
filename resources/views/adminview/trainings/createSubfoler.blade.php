@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Create Sub-Title" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="#" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <select class="form-select shadow-none" name="content_id" id="content_id"
                                        aria-label="Content">
                                        <option selected> -- Select--</option>
                                        @foreach ($contents as $content)
                                        <option value="{{$content->id}}">{{$content->name}}</option>

                                        @endforeach

                                    </select>
                                    <label for="content_id">Content <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="name" id="name"
                                        placeholder="Name" readonly>
                                    <label for="name">Name <span>*</span> </label>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-floating d-flex justify-content-start align-items-center">
                                    <button type="submit" class="btn btn-primary btn-get-started">Create <i
                                            class="bi bi-upload"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(document).ready(function(){
    $('#title').change(function(e){
        $.get('{{ route("checkSlugDoc") }}',
            {'title':$(this).val(),},
            function (data){
                $('#slug').val(data.slug);
            }
        );
    });
    });

</script>
@endsection