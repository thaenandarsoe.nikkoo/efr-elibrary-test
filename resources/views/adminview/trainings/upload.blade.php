@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Upload Trainings" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="{{route('trainings.store')}}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="file_name" id="file_name"
                                        required placeholder="File Name">
                                    <label for="file_name">File Name <span>*</span> </label>
                                </div>
                                <x-backend.form_error name="file_name" />
                            </div>

                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="slug" id="slug"
                                        placeholder="Slug" readonly>
                                    <label for="slug">Slug <span>*</span> </label>
                                </div>
                            </div>
                            <x-backend.form_error name="slug" />

                            <input type="hidden" name="title" value="{{request()->title}}">




                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="file" class="form-control shadow-none" name="file" id="file"
                                        placeholder="File" required>
                                </div>
                                <x-backend.form_error name="file" />
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating mb-3">
                                    <select class="form-select shadow-none" name="sub_folder_id" id="sub_folder_id"
                                        aria-label="Sub Title">
                                        <option value="" selected> -- Select--</option>

                                        @foreach ($subfolders as $sub)
                                        <option value="{{$sub->id}}">{{$sub->name}}</option>

                                        @endforeach
                                    </select>
                                    <label for="sub_folder_id">Sub Title </label>
                                </div>
                                <x-backend.form_error name="sub_folder_id" />
                            </div>

                            <div class="col-md-12">
                                <div class="form-floating">
                                    <textarea class="form-control shadow-none" placeholder="Description"
                                        name="description" id="description" style="height: 150px;"></textarea>
                                    <label for="description">Description </label>
                                </div>
                                <x-backend.form_error name="description" />
                            </div>


                            <div class="col-md-4">
                                <div class="form-floating ">
                                    <button type="submit" class="btn btn-primary btn-get-started">Upload <i
                                            class="bi bi-upload"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(document).ready(function(){
    $('#file_name').change(function(e){
        $.get('{{ route("checkSlugTraining") }}',
            {'file_name':$(this).val(),},
            function (data){
                $('#slug').val(data.slug);
            }
        );
    });
    });

</script>
@endsection