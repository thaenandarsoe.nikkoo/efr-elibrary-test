@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Upload Trainings" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="{{route('trainings.create')}}" method="get">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <select class="form-select shadow-none" name="title" id="title"
                                        aria-label="Sub Title">
                                        <option value="" selected> -- Select--</option>

                                        @foreach ($titles as $title)
                                        <option value="{{$title->id}}">{{$title->name}}</option>

                                        @endforeach
                                    </select>
                                    <label for="title">Title <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-floating ">
                                    <button type="submit" class="btn btn-primary btn-get-started">Next <i
                                            class="bi bi-arrow-right"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@endsection