@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Document Details" />

    <section class="section">
        <div class="container result-container">
            <div class="row">
                <div class="card my-4 p-4">

                    <div class="col-md-12 service-box text-start " data-aos="fade-up" data-aos-delay="200">
                        {{-- <i class="ri-discuss-line icon"></i> --}}

                        <p class="detail-para"> <strong>ID -</strong> {{$document->id}}</p>

                        <p class="detail-para"> <strong>Title -</strong> {{$document->title}}</p>

                        <p class="detail-para"> <strong>Slug -</strong> {{$document->slug}} </p>

                        <p class="detail-para"> <strong>Description -</strong> {{$document->description}} </p>

                        <p class="detail-para"> <strong>Category -</strong> {{$document->content->name}} </p>

                        <p class="detail-para"> <strong>Uploaded by -</strong> {{$document->uploaded_by}} </p>

                        <p class="detail-para"> <strong>Created at -</strong> {{$document->created_at->format('d-M-Y
                            H:m:s')}} </p>

                        <p class="detail-para"> <strong>Latest Edited at -</strong>
                            {{$document->updated_at->format('d-M-Y H:m:s')}} </p>

                        <p class="detail-para"> <strong>Link -</strong> <a href="{{$document->link}}"
                                target="_blank">{{$document->link}}</a>
                        </p>

                    </div>

                    <div class="col-md-4">

                        <a href="{{route('documents.index')}}"><button type="submit" class="btn btn-success "> <i
                                    class="bi bi-list-ul"></i></button></a>

                        <button type="submit" class="btn btn-primary "> <i class="bi bi-pencil"></i></button>

                        <button type="submit" class="btn btn-danger"> <i class="bi bi-trash"></i></button>

                    </div>
                </div>
            </div>
        </div>
    </section>



</main><!-- End #main -->
@endsection