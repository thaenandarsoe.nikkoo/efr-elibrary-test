@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Documents" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <button class="btn btn-sm  shadow-none"><input type="checkbox" name="" id=""></button>
                        <button class="btn btn-sm btn-info shadow-none"> <i class="bi bi-check2-square"></i> 0
                            selected</button>
                        <button class="btn btn-sm btn-danger shadow-none"> <i class="bi bi-trash"></i> Bulk
                            Delete</button>

                        <div class="table-responsive">
                            <!-- Table with stripped rows -->
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Date Create</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($documents as $document)
                                    <tr>
                                        <th scope="row">{{$document->id}}</th>
                                        <td> <a href="{{$document->link}}" target="_blank">{{$document->title}}</a>
                                        </td>
                                        <td>{{$document->content->name}}</td>
                                        <td>{{$document->description}}</td>

                                        <td>{{$document->created_at}}</td>
                                        <td>
                                            <span class="btn btn-sm btn-success mb-3"><i
                                                    class="bi bi-pencil-square"></i>
                                            </span>
                                            <a href="{{route('documents.show',$document->id)}}"><span
                                                    class="btn btn-sm btn-warning mb-3"><i
                                                        class="bi bi-eye"></i></span></a>
                                            <span class="btn btn-sm btn-danger mb-3"><i class="bi bi-trash"></i></span>
                                            {{-- <span class="badge bg-warning text-dark">View</span> --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- End Table with stripped rows -->
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->
@endsection
