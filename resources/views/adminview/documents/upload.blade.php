@extends('components.backend.layout')
@section('content')
<x-backend.header />
<x-backend.sidebar />

<main id="main" class="main">

    <x-backend.pagetitle title="Upload Document" />

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body py-5">

                        <!-- Floating Labels Form -->
                        <form class="row g-3" action="{{route('documents.store')}}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="title" id="title" required
                                        placeholder="Title">
                                    <label for="title">Title <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control shadow-none" name="slug" id="slug"
                                        placeholder="Slug" readonly>
                                    <label for="slug">Slug <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- <div class="form-floating">
                                    <input type="file" class="form-control shadow-none" name="file" id="file"
                                        placeholder="File">

                                </div> --}}
                                <div class="form-floating">
                                    <input type="url" class="form-control shadow-none" name="link" id="link"
                                        placeholder="Link" required>

                                    <label for="link">OneDrive Link <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating mb-3">
                                    <select class="form-select shadow-none" name="content_id" id="content_id"
                                        aria-label="Content">
                                        <option selected> -- Select--</option>
                                        @foreach ($contents as $content)
                                        <option value="{{$content->id}}">{{$content->name}}</option>

                                        @endforeach
                                    </select>
                                    <label for="content_id">Content <span>*</span> </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-floating">
                                    <button type="submit" class="btn btn-primary btn-get-started">Upload <i
                                            class="bi bi-upload"></i></button>
                                </div>
                            </div>

                        </form><!-- End floating Labels Form -->

                    </div>
                </div>

            </div>
        </div>
    </section>

</main><!-- End #main -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(document).ready(function(){
    $('#title').change(function(e){
        $.get('{{ route("checkSlugDoc") }}',
            {'title':$(this).val(),},
            function (data){
                $('#slug').val(data.slug);
            }
        );
    });
    });

</script>
@endsection