@extends('components.frontend.layout')
@section('content')
<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="index.html" class="logo d-flex align-items-center">
            <!-- <img src="assets/img/logo/.png" alt="" /> -->
            <!-- <span>E.F.R</span> -->
        </a>

        <nav id="navbar" class="navbar">
            <!-- <ul>
            <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
            <li><a class="nav-link scrollto" href="#about">About</a></li>
            <li><a class="nav-link scrollto" href="#services">Services</a></li>
            <li>
              <a class="nav-link scrollto" href="#portfolio">Portfolio</a>
            </li>
            <li><a class="nav-link scrollto" href="#team">Team</a></li>
            <li><a href="blog.html">Blog</a></li>

            <li>
              <a class="getstarted scrollto" href="#about">Get Started</a>
            </li>
          </ul> -->
            <i class="mobile-nav-toggle"></i>
        </nav>
        <!-- .navbar -->
    </div>
</header>
<!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center justify-content-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <x-frontend.groupTitle />
                <h5 data-aos="fade-up" data-aos-delay="400">
                    This system offers some digital resources,
                    collection of books, articles, research papers, and multimedia
                    content.
                </h5>
                <div data-aos="fade-up" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <a href="#about"
                            class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center"
                            data-bs-toggle="modal" data-bs-target="#loginModal">
                            <span>Get Started</span>
                            <i class="bi bi-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->

<main id="main">
    <!-- Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header px-5">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Please Sign in First
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-5">
                    <form action="" method="">
                        <div class="mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" class="form-control shadow-none" name="email" id="email"
                                aria-describedby="emailHelp" />
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control shadow-none" name="password" id="password" />
                        </div>
                        <div class="mb-3">
                            <div class="form-check">
                                <input class="form-check-input shadow-none" type="checkbox" value="" id="agree"
                                    required />
                                <label class="form-check-label" for="flexCheckDefault">
                                    Respect to copyright. Agree not to distribute without permissions.
                                </label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <button type="submit" class="btn btn-get-started">
                                <span>Sign in</span> <i class="bi bi-box-arrow-in-right"></i>
                            </button>

                        </div>
                    </form>
                    <small>New to this? Contact to admin - +959 958413515 (or)
                        admin@example.com</small>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- End #main -->
<x-frontend.searchResultModal />
@endsection