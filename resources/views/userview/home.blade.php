@extends('components.frontend.layout')
@section('content')
<x-frontend.header />
<x-frontend.hero />


<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">
            <header class="section-header">
                <!-- <h2>Categories</h2> -->
                <p>Categories</p>
            </header>
            <div class="d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-6 mat-5 mt-lg-0 " data-aos="fade-up" data-aos-delay="200">


                            <div class="service-box mt-4">
                                {{-- <i class="ri-discuss-line icon"></i> --}}
                                <p class="title">Trainings</p>


                                <a href="{{url('/trainings-lists')}}" class="read-more"><span>
                                        View More</span> <i class="bi bi-arrow-right"></i></a>

                                {{--
                                <x-frontend.categoriesModal :category_id="$category->id"
                                    :category_name="$category->name" />
                                --}}

                                {{--
                                <x-frontend.categoriesLists :categories="$categories" /> --}}


                            </div>
                        </div>
                        <div class="col-lg-6 mat-5 mt-lg-0 " data-aos="fade-up" data-aos-delay="200">

                            <div class="service-box mt-4">
                                {{-- <i class="ri-discuss-line icon"></i> --}}
                                <p class="title">Documents</p>


                                <a href="{{url('/documents-lists')}}" class="read-more"><span>
                                        View More</span> <i class="bi bi-arrow-right"></i></a>

                                {{--
                                <x-frontend.categoriesModal :category_id="$category->id"
                                    :category_name="$category->name" />
                                --}}


                                {{--
                                <x-frontend.categoriesLists :categories="$categories" /> --}}


                            </div>
                        </div>
                    </div>
                </div>
                <!-- / row -->
            </div>
        </div>
    </section>
    <!-- End Features Section -->
</main>
<!-- End #main -->
@endsection