@extends('components.frontend.layout')
@section('content')
<x-frontend.header />

<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="contents" class="contents services">
        <div class="container" data-aos="fade-up">
            <header class="section-header">
                <!-- <h2>Categories</h2> -->
                <p>Contents</p>
            </header>

            <div class="row">

                <div class="col-lg-12 mt-5 mt-lg-0 d-flex">
                    <div class="container result-container">

                        <div class="row my-3 d-flex justify-content-center align-items-center">


                            @foreach ($contents as $content)

                            <div class="col-lg-8 col-md-12 col-sm-12" data-aos="fade-up" data-aos-delay="200">
                                <div class="service-box text-start p-4 mt-4">
                                    {{-- <i class="ri-discuss-line icon"></i> --}}
                                    <a href="{{$content->link}}" class="title">
                                        <p>{{$content->title}}</p>
                                    </a>


                                    <div class="row">
                                        <p>{{$content->description}}</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <small class="small"><strong>Category -</strong>
                                                {{$content->category->name}}
                                                <!--Main Category-->
                                            </small>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <small class="small"><strong>Type -</strong> {{$content->type}}
                                                <!--Main Category-->
                                            </small>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <small class="small"><strong>Created at -</strong>
                                                {{$content->created_at->format('d-M-y')}}</small>
                                        </div>

                                        <div class="col-md-4 my-2">
                                            <a href="{{$content->link}}" target="_blank"
                                                class="learn d-inline-flex align-items-center justify-content-center align-self-center">
                                                <span>Documents</span>
                                                <i class="bi bi-arrow-right"></i>
                                            </a>
                                        </div>

                                        <div class="col-md-4 my-2">
                                            <a href="#" target="_blank"
                                                class="learn d-inline-flex align-items-center justify-content-center align-self-center">
                                                <span>Videos</span>
                                                <i class="bi bi-arrow-right"></i>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>


                    </div>
                </div>
            </div>
            <!-- / row -->
        </div>
    </section>
    <!-- End Features Section -->
</main>
<!-- End #main -->
@endsection