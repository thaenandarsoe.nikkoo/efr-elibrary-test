@extends('components.frontend.layout')
@section('content')
<x-frontend.header />

<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">
            <header class="section-header">
                <!-- <h2>Categories</h2> -->
                <p>Title of the content</p>
            </header>

            <div class="row">


                <div class="col-lg-12 mt-5 mt-lg-0 d-flex">
                    <div class="container">
                        <div class="row">

                            <ul class="list-group">

                                @foreach ($files as $file)

                                {{-- @dd($file) --}}
                                <li class="list-group-item">
                                    {{$file->getBasename()}}
                                </li>

                                {{-- <li class="list-group-item"> <a
                                        href="{{asset('/storage/media/'.$file->getFilename())}}">{{$file->getFilename()}}</a>
                                </li> --}}

                                @endforeach


                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- / row -->
        </div>
    </section>
    <!-- End Features Section -->
</main>

@endsection