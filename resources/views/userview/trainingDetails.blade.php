@extends('components.frontend.layout')
@section('content')
<x-frontend.header />


<main id="main">
    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq">

        <div class="container mt-5" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-12 trainingDetails">
                    <div class="trainings-accordion p-2 mx-4 border-0 mt-2">
                        <!--List 1-->
                        <div class="accordion accordion-flush" id="faqlist1">
                            <h3 class="mx-lg-5">{{$content->name}}</h3>

                            {{-- if no subfolder --}}

                            {{-- end if no sub folder --}}

                            @foreach ($subfolders as $index=>$sub)

                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#{{$sub->slug}}">
                                        <i class="bi bi-chevron-right"></i> {{$sub->name}}
                                    </button>
                                </h2>
                                <div id="{{$sub->slug}}"
                                    class="accordion-collapse collapse {{$index == 0 ? 'show' : ''}}"
                                    data-bs-parent="#faqlist1">
                                    <div class="accordion-body">
                                        <div class="row">
                                            <div class="col-md-3">

                                                <!-- Tabs nav -->
                                                <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab"
                                                    role="tablist" aria-orientation="vertical">

                                                    @foreach ($trainings as $t=>$training)


                                                    @if ($training->sub_folder_id == $sub->id)

                                                    <a class="nav-link mb-3 p-3 shadow {{$t == 0 ? 'active' : ''}}"
                                                        id="v-pills-home-tab" data-bs-toggle="pill"
                                                        href="#{{$sub->slug}}{{$training->slug}}" role="tab"
                                                        aria-selected="true">
                                                        <i class="bi bi-play-btn-fill mr-2"></i>
                                                        <span
                                                            class="font-weight-bold small text-uppercase">{{$training->file_name}}</span></a>


                                                    @endif
                                                    @endforeach




                                                </div>
                                            </div>


                                            <div class="col-md-9">
                                                <!-- Tabs content -->
                                                <div class="tab-content" id="v-pills-tabContent">

                                                    @foreach ($trainings as $tr=>$training)


                                                    @if ($training->sub_folder_id == $sub->id)
                                                    <div class="tab-pane fade shadow rounded bg-white show {{$tr== 0 ? 'active' : ''}} p-5"
                                                        id="{{$sub->slug}}{{$training->slug}}" role="tabpanel">
                                                        <h4 class="font-italic mb-4">{{$training->file_name}}</h4>
                                                        <div class="card border-0 m-2 shadow-lg videocard">

                                                            <video class="video-js" oncontextmenu="return false;"
                                                                controls preload="
                                                                metadata" height="400px" controlslist="nodownload ">
                                                                <source src="{{asset('/storage/'.$training->file)}}"
                                                                    type="video/mp4">
                                                                <source src="{{asset('/storage/'.$training->file)}}"
                                                                    type="video/webm">
                                                                <source src="{{asset('/storage/'.$training->file)}}"
                                                                    type="video/ogg">
                                                            </video>

                                                        </div>
                                                    </div>

                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </div>
                    </div>
                </div>



            </div>

        </div>

    </section><!-- End F.A.Q Section -->
</main>

<script>
    const videos = document.querySelectorAll('video');
    const navLinks = document.querySelectorAll('.nav-link');
    const tabPanes = document.querySelectorAll('.tab-pane');



videos.forEach(video => {
video.addEventListener('play', (event) => {
pauseOtherVideos(event.target);
});

video.addEventListener('ended', (event) => {
playNextVideo(event.target);
});
});

function pauseOtherVideos(currentVideo) {
videos.forEach(video => {
if (video !== currentVideo && !video.paused) {
video.pause();
}
});
}function playNextVideo(currentVideo) {
const currentIndex = Array.from(videos).indexOf(currentVideo);
const nextIndex = (currentIndex + 1) % videos.length;
const nextVideo = videos[nextIndex];
const nextNavLink = navLinks[nextIndex];
const nextTabPane = tabPanes[nextIndex];


pauseOtherVideos(currentVideo);

if (nextVideo) {
nextVideo.play();
navLinks.forEach(navLink => navLink.classList.remove('active'));
nextNavLink.classList.toggle('active','show');
tabPanes.forEach(tabPane => tabPane.classList.remove('show', 'active'));
nextTabPane.classList.add('show', 'active');
}
}

</script>


@endsection