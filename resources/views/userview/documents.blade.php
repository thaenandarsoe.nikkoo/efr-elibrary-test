@extends('components.frontend.layout')
@section('content')
<!-- Vendor CSS Files -->

<link href="{{asset('backend/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">

<x-frontend.header />



<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="contents" class="contents services">
        <div class="container" data-aos="fade-up">
            <header class="section-header">
                <!-- <h2>Categories</h2> -->
                <p>Documents</p>
            </header>

            <div class="row">

                <div class="col-lg-12 mt-5 mt-lg-0 d-flex">
                    <div class="container result-container">

                        <div class="row my-3 d-flex justify-content-center align-items-center">

                            <div class="table-responsive">
                                <!-- Table with stripped rows -->
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>

                                            <th scope="col">Content</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Date Create</th>
                                            <th scope="col">Link</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($documents as $document)
                                        <tr>
                                            <th scope="row">{{$document->id}}</th>
                                            <td> <a href="{{$document->link}}" target="_blank">{{$document->title}}</a>
                                            </td>
                                            <td>{{$document->content->name}}</td>
                                            <td>{{$document->description}}</td>

                                            <td>{{$document->created_at->format('d-M-y')}}</td>
                                            <td>

                                                <a href="{{$document->link}}" target="_blank"><span
                                                        class="btn btn-sm btn-primary mb-3"><i
                                                            class="bi bi-arrow-right"></i></span></a>

                                                {{-- <span class="badge bg-warning text-dark">View</span> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- End Table with stripped rows -->
                            </div>
                            {{-- @foreach ($documents as $document)

                            <div class="col-lg-8 col-md-12 col-sm-12" data-aos="fade-up" data-aos-delay="200">
                                <div class="service-box text-start p-4 mt-4">

                                    <a href="{{$document->link}}" class="title">
                                        <p>{{$document->title}}</p>
                                    </a>


                                    <div class="row">
                                        <p>{{$document->description}}</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <small class="small"><strong>Category -</strong>
                                                {{$document->content->name}}

                                            </small>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <small class="small"><strong>Created at -</strong>
                                                {{$document->created_at->format('d-M-y')}}</small>
                                        </div>

                                        <div class="col-md-12 my-2">
                                            <a href="{{$document->link}}" target="_blank"
                                                class="learn d-inline-flex align-items-center justify-content-end align-self-center">
                                                <span>Learn</span>
                                                <i class="bi bi-arrow-right"></i>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach --}}


                        </div>


                    </div>
                </div>
            </div>
            <!-- / row -->
        </div>
    </section>
    <!-- End Features Section -->
</main>
<!-- End #main -->

<!-- Vendor JS Files -->
<script src="{{asset('backend/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{asset('backend/assets/vendor/tinymce/tinymce.min.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('backend/assets/js/main.js')}}"></script>
@endsection