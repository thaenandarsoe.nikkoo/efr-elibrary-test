@extends('components.frontend.layout')
@section('content')
<!-- Vendor CSS Files -->

<link href="{{asset('backend/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">

<x-frontend.header />



<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="contents" class="contents services">
        <div class="container" data-aos="fade-up">
            <header class="section-header">
                <!-- <h2>Categories</h2> -->
                <p>Trainings</p>
            </header>

            <div class="row d-flex justify-content-center">

                <div class="col-lg-10 col-md-12 mt-5 mt-lg-0 d-flex">
                    <div class="container result-container">

                        <div class="row my-3 d-flex justify-content-center align-items-center">

                            <div class="table-responsive">
                                <!-- Table with stripped rows -->
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Contents</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Date Create</th>
                                            <th scope="col">Learn</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($trainings->unique('content_id') as $training)
                                        <tr>
                                            <td>{{$training->id}}</td>
                                            <td>{{$training->content->name}}</td>
                                            <td>{{$training->description}}</td>
                                            <td>{{$training->created_at->format('d-M-y')}}</td>
                                            <td>
                                                <a href="{{url('/trainings-details/'.$training->content->id)}}"><span
                                                        class="btn btn-sm btn-primary mb-3"><i
                                                            class="bi bi-arrow-right"></i></span></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- End Table with stripped rows -->
                            </div>
                            


                        </div>


                    </div>
                </div>
            </div>
            <!-- / row -->
        </div>
    </section>
    <!-- End Features Section -->
</main>
<!-- End #main -->

<!-- Vendor JS Files -->
<script src="{{asset('backend/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{asset('backend/assets/vendor/tinymce/tinymce.min.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('backend/assets/js/main.js')}}"></script>
@endsection
