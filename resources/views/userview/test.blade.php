@extends('components.frontend.layout')
@section('content')
<x-frontend.header />


<main id="main">
    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq">

        <div class="container mt-5" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-12 trainingDetails">
                    <div class="row">
                        <div class="col-md-8">

                            <div class="tab-content" id="v-pills-tabContent">

                                @foreach ($trainings as $training)
                                <div class="tab-pane fade shadow rounded bg-white {{ $loop->first ? ' show active' : '' }} p-5"
                                    id="{{$training->slug}}{{$training->id}}" role="tabpanel">

                                    <h4 class="font-italic mb-4">{{$training->file_name}}</h4>
                                    <div class="card border-0 m-2 shadow-lg videocard">

                                        <video class="video-js training-video" oncontextmenu="return false;" autoplay
                                            controls preload="
                                                                                                            metadata"
                                            height="400px" controlslist="nodownload ">
                                            <source src="{{asset('/storage/'.$training->file)}}" type="video/mp4">

                                        </video>

                                    </div>
                                </div>


                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-4 nav-cont p-5">

                            <!-- Tabs nav -->
                            <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist"
                                aria-orientation="vertical">

                                @foreach ($trainings as $training)

                                <a class="nav-link training-tab-link mb-3 p-3 shadow {{ $loop->first ? ' active' : '' }}"
                                    id="" data-bs-toggle="pill" href="#{{$training->slug}}{{$training->id}}" role="tab"
                                    aria-selected="true">

                                    <p class="font-weight-bold small text-uppercase">
                                        <i class="bi bi-play-btn-fill mr-2"></i>
                                        <small class="sub_folder_name"> {{$training->subFolders->name}}</small>
                                    </p>
                                    <p>{{$training->file_name}}</p>


                                </a>

                                @endforeach

                            </div>
                        </div>


                    </div>
                </div>




            </div>

        </div>

    </section><!-- End F.A.Q Section -->
</main>


<script>
    const videos = document.querySelectorAll('video');
    const navLinks = document.querySelectorAll('.training-tab-link');
    const tabPanes = document.querySelectorAll('.tab-pane');
    const navContainer = document.querySelector('.nav-cont');

    videos.forEach(video => {
        video.addEventListener('play', (event) => {
            pauseOtherVideos(event.target);
        });

        video.addEventListener('ended', (event) => {
            playNextVideo(event.target);
        });
    });

    function pauseOtherVideos(currentVideo) {
        videos.forEach(video => {
            if (video !== currentVideo && !video.paused) {
                video.pause();
            }
        });
    }

    function playNextVideo(currentVideo) {
        const currentIndex = Array.from(videos).indexOf(currentVideo);
        const nextIndex = (currentIndex + 1) % videos.length;
        const nextVideo = videos[nextIndex];
        const nextNavLink = navLinks[nextIndex];

        const nextTabPane = tabPanes[nextIndex];

        pauseOtherVideos(currentVideo);

        if (nextVideo) {
            nextVideo.play();

            navLinks.forEach(navLink => navLink.classList.remove('active'));
            nextNavLink.classList.add('active');

            tabPanes.forEach(tabPane => tabPane.classList.remove('show', 'active'));
            nextTabPane.classList.add('show', 'active');

            // Scroll the active nav link into view if necessary

            scrollToActiveNavLink(nextNavLink);
        }
    }
   function scrollToActiveNavLink(navLink) {
        const navContainerRect = navContainer.getBoundingClientRect();
        const navLinkRect = navLink.getBoundingClientRect();

        // Check if the nav link is below the visible area
        if (navLinkRect.bottom > navContainerRect.bottom) {
        const scrollAmount = navLinkRect.bottom - navContainerRect.bottom;
        navContainer.scrollTop += scrollAmount;
        }

        // Check if the nav link is above the visible area
        if (navLinkRect.top < navContainerRect.top) { const scrollAmount=navContainerRect.top - navLinkRect.top;
            navContainer.scrollTop -=scrollAmount; }
    }
</script>

@endsection