@props(['categories'])
@foreach ($categories as $category)

<div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
    <div class="service-box mt-4">
        {{-- <i class="ri-discuss-line icon"></i> --}}
        <p class="title">{{$category->name}}</p>


        <a href="{{url('/category-contents/'.$category->id)}}" class="read-more"><span>
                View More</span> <i class="bi bi-arrow-right"></i></a>

        {{--
        <x-frontend.categoriesModal :category_id="$category->id" :category_name="$category->name" /> --}}
    </div>
</div>
@endforeach