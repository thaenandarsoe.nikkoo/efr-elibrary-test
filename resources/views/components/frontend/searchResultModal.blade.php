@if (request('search'))

<div id="result-container" class="result-container shadow-lg">
    <div class="overlay shadow-lg"></div>
    <div class="res-container">
        <div class="close shadow-lg" id="close-btn">
            x
        </div>

        {{-- @if (count($searchResult) > 0) --}}

        <div class="my-1">
            <header class="section-header pt-4">
                <h2>Search Results</h2>
                {{-- <p>Search Results</p> --}}
            </header>
            <x-frontend.results />
        </div>
        {{-- @else
        <p style="color:#000">No result here....</p>
        @endif --}}
    </div>
</div>

@endif

<script>
    const closeModalButton = document.getElementById('close-btn');
        closeModalButton.addEventListener('click', function() {
            const modal = document.querySelector('.result-container');
            modal.classList.add('close_modal');
            javascript: window.location.href = "{{ Request::url() }}";
        });
</script>