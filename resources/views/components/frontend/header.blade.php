<!-- ======= Header ======= -->
<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="{{url('/')}}" target="_blank" class="logo d-flex align-items-center">
            <img src="assets/img/logo.png" alt="" />
            <span>DEMO</span>
        </a>
        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto" href="{{url('/home')}}">Home</a></li>
                <li><a class="nav-link scrollto" href="{{url('/all-contents')}}">Contents</a></li>
                <li><a class="nav-link scrollto" data-bs-toggle="modal" data-bs-target="#help" href="#">Need
                        Help?</a></li>

                <li class="dropdown">
                    <a href="#"><span>Thae Nandar Soe</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="{{url('/admin/dashboard')}}">Administrations</a></li>
                        <li><a href="{{url('/')}}">Sign Out</a></li>
                    </ul>
                </li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
        <!-- .navbar -->

        <!-- .navbar -->
    </div>
</header>
<!-- End Header -->

<!-- Need Help Modal -->
<div class="modal fade" id="help" tabindex="-1" aria-labelledby="helpModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="helpModalLabel">Need Help?Please contact the administrators described
                    below.</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <!-- ======= Contact Section ======= -->

                <div class="container contact" data-aos="fade-up">

                    <div class="row gy-4">

                        <div class="col-md-6 col-sm-12">
                            <div class="info-box">
                                <i class="bi bi-telephone"></i>
                                <h3>Call Us (or) Via Viber</h3>
                                <p> <strong>Person 1</strong> : +1 5589 55488 55</p>
                                <p> <strong>Person 2</strong> : +1 6678 254445 41</p>
                                {{-- <p> <strong>Khin Phyu Kyi</strong> : +1 5589 55488 55</p>
                                <p> <strong>Theingi Tun</strong> : +1 6678 254445 41</p> --}}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="info-box">
                                <i class="bi bi-envelope"></i>
                                <h3>Email Us</h3>
                                <p><strong>Person 1</strong> : admin@example.com</p>
                                <p> <strong>Person 2</strong> : admin@example.com</p>
                            </div>
                        </div>

                    </div>

                </div>


            </div>

        </div>
    </div>
</div>