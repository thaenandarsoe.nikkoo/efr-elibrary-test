<!-- ======= Hero Section ======= -->
<section id="sechero" class="sechero d-flex align-items-center justify-content-center">
    <div class="container justify-content-center">
        <div class="row justify-content-center">

            <div class="col-lg-6">
                <x-frontend.groupTitle />

                <div data-aos="fade-up" class="mt-5" data-aos-delay="600">
                    <div class="text-center text-lg-start">
                        <div class="search col-md-10">
                            <form action="" method="get">
                                <div class="input-group mb-3">
                                    <input type="text" name="search" class="form-control shadow-lg py-3"
                                        placeholder="-- Search --" size="60" />
                                    <button class="btn btn-primary px-3" data-bs-toggle="modal"
                                        data-bs-target="#searchResultModal" type="submit" id="button-addon2">
                                        <i class="bi bi-search"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->
<main>
    <x-frontend.searchResultModal />
</main>