@props(['category_id','category_name'])
<!-- Category Modal will appear when view more link from category box is clicked-->
<!-- Modal -->
<div class="modal fade" id="viewmore{{$category_id}}" tabindex="-1" aria-labelledby="viewmore" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewmoreModalLabel">{{$category_name}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <div class="container result-container" data-aos="fade-up">

                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Result accordion List 1-->
                            <div class="accordion" id="faqlist1">
                                <x-frontend.resultItem />
                                <x-frontend.resultItem />
                                <x-frontend.resultItem />
                                <x-frontend.resultItem />
                                <x-frontend.resultItem />


                            </div>
                        </div>

                        <div class="col-lg-6">

                            <!--Result accordion List 2-->
                            <div class="accordion" id="faqlist2">

                                <x-frontend.resultItem />
                                <x-frontend.resultItem />
                                <x-frontend.resultItem />

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>