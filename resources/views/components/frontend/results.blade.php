<!-- ======= Result Section ======= -->
<div id="results" class="results">

    <div class="container" data-aos="fade-up">

        <div class="row">
            <div class="col-lg-6">
                <!-- Result accordion List 1-->
                <div class="accordion" id="faqlist1">
                    <x-frontend.resultItem />
                    <x-frontend.resultItem />
                    <x-frontend.resultItem />
                    <x-frontend.resultItem />
                    <x-frontend.resultItem />


                </div>
            </div>

            <div class="col-lg-6">

                <!--Result accordion List 2-->
                <div class="accordion" id="faqlist2">

                    <x-frontend.resultItem />
                    <x-frontend.resultItem />
                    <x-frontend.resultItem />

                </div>
            </div>

        </div>



    </div>

</div><!-- End Result -->