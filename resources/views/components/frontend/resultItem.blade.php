<div class="accordion-item mt-3">
    <h2 class="accordion-header">
        <a href="#"><button class="accordion-button" type="button">
                Training
                <!--Title-->
            </button></a>
    </h2>
    <div id="faq2-content-1" class="accordion-collapse" data-bs-parent="#faqlist2">
        <div class="accordion-body text-start">
            <div class="row">
                <p>this is description of training.</p>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <small class="small">Category - Training
                        <!--Main Category-->
                    </small>
                </div>
                <div class="col-md-6 col-sm-12">
                    <small class="small">Created at - {{today()->format('d-M-y')}}</small>
                </div>

                <div class="col-md-6 col-sm-12">
                    <a href="https://efrgroupmyanmar-my.sharepoint.com/personal/it-executive_efrgroupmm_com/_layouts/15/onedrive.aspx?csf=1&web=1&e=i8ZpiY&cid=f78e2f9b%2Daa55%2D4b40%2Db29d%2Dfd74b02ee2fe&id=%2Fpersonal%2Fit%2Dexecutive%5Fefrgroupmm%5Fcom%2FDocuments%2FTesting%20Links%2FTesting%20sub%20folder&FolderCTID=0x012000B07B622617359D44841D0C46131A1B21&view=0"
                        class="learn d-inline-flex align-items-center justify-content-center align-self-center">
                        <span>Learn</span>
                        <i class="bi bi-arrow-right"></i>
                    </a>
                </div>

            </div>

        </div>
    </div>
</div>