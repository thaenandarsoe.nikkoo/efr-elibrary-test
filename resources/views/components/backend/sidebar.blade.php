<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="{{url('/admin/dashboard')}}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-heading">Pages</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
                <i class="bi bi-journal-text"></i><span>Documents</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">

                <li>
                    <a href="{{route('documents.index')}}">
                        <i class="bi bi-circle"></i><span>Show</span>
                    </a>
                    <a href="{{route('documents.create')}}">
                        <i class="bi bi-circle"></i><span>Upload</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#trainings" data-bs-toggle="collapse" href="#">
                <i class="bi bi-skip-end-circle"></i><span>Trainings</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="trainings" class="nav-content collapse " data-bs-parent="#sidebar-nav">

                <li>
                    <a href="{{route('trainings.index')}}">
                        <i class="bi bi-circle"></i><span>Show</span>
                    </a>
                    <a href="{{url('/admin/train/choosetitle')}}">
                        <i class="bi bi-circle"></i><span>Upload</span>
                    </a>
                    <a href="{{url('/admin/courses/create/sub-folder')}}">
                        <i class="bi bi-circle"></i><span>Create Sub-Folder</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#tag" data-bs-toggle="collapse" href="#">
                <i class="bi bi-tag"></i><span>Contents</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="tag" class="nav-content collapse " data-bs-parent="#sidebar-nav">

                <li>
                    <a href="{{route('contents.index')}}">
                        <i class="bi bi-circle"></i><span>Show</span>
                    </a>
                    <a href="{{route('contents.create')}}">
                        <i class="bi bi-circle"></i><span>Upload</span>
                    </a>
                </li>
            </ul>
        </li>


        <li class="nav-heading">Users & Admins</li>

        <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#users" data-bs-toggle="collapse" href="#">
                <i class="bi bi-person"></i><span>Users</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="users" class="nav-content collapse " data-bs-parent="#sidebar-nav">

                <li>
                    <a href="{{route('users.index')}}">
                        <i class="bi bi-circle"></i><span>Show</span>
                    </a>
                    <a href="{{route('users.create')}}">
                        <i class="bi bi-circle"></i><span>Register</span>
                    </a>
                </li>
            </ul>
        </li>


        {{-- <li class="nav-item">
            <a class="nav-link collapsed" href="pages-error-404.html">
                <i class="bi bi-people"></i>
                <span>Users</span>
            </a>
        </li> --}}



    </ul>

</aside><!-- End Sidebar-->