<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\SubFolder;
use App\Models\Training;
use Illuminate\Http\Request;

use \Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Validation\Rule;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('adminview.trainings.upload', [
            'subfolders'=>SubFolder::where('content_id', request()->title)->get(),
        ]);

    }

    public function choosetitle()
    {
        return view('adminview.trainings.chooseTitle', [
             'titles'=>Content::where('type', 'trainings')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $training = $request->validate([
             'file_name' => ['required', 'min:4'],
            'slug' => ['required'],
            'file'=> ['required'],
         ]);


        $title = Content::select('slug')->where('id', $request->title)->first();

        if($request->sub_folder_id == null) {
            $store_path = "trainings/$title[slug]";
        } else {
            $sub_folder = SubFolder::select('slug')->where('id', $request->sub_folder_id)->first();
            $store_path = "trainings/$title[slug]/$sub_folder[slug]";

        }
        $training['content_id'] = $request->title;
        $training['description'] = $request->description;
        $training['sub_folder_id'] = $request->sub_folder_id;
        $training['uploaded_by'] = 'thae nandar soe';

        $file_name = request()->file('file')->getClientOriginalName();


        $training['file'] = request()->file('file')->storeAs("$store_path", $file_name) ;

        Training::create($training);

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     */
    public function show(Training $training)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Training $training)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Training $training)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Training $training)
    {
        //
    }

    public function createSubFolder()
    {
        return view('adminview.trainings.createSubfoler', [
            'contents' => Content::where('type', 'trainings')->get(),
        ]);
        // return view('adminview.trainings.createSubfoler', [
        //     'trainings' => Training::all(),
        // ]);
    }
    public function checkSlugTraining(Request $request)
    {

        $slug = SlugService::createSlug(Training::class, 'slug', $request->file_name);
        return response()->json(['slug'=>$slug]);
    }
}
