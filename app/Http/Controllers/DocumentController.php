<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\Document;
use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('adminview.documents.show', [
            'documents' =>Document::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('adminview.documents.upload', [
            'contents' => Content::all(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // return $request;
        Document::create([
          'title' => $request->title,
          'description' => $request->description,
          'slug' => $request->slug,
          'content_id' => $request->content_id,
          'link' => $request->link,
          'uploaded_by'=>'thaenandarsoe',
        ]);
        return redirect()->route('documents.index');

    }

    /**
     * Display the specified resource.
     */
    public function show(Document $document)
    {
        return view('adminview.documents.details', [
          'document' => $document,
      ]);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Document $document)
    {
        //
    }
    public function checkSlugDoc(Request $request)
    {

        $slug = SlugService::createSlug(Document::class, 'slug', $request->title);
        return response()->json(['slug'=>$slug]);
    }
}
