<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Content;
use App\Models\Document;
use App\Models\SubFolder;
use App\Models\Training;
use Illuminate\Http\Request;

class UserviewController extends Controller
{
    public function index()
    {
        return view('index');

    }
    public function home()
    {
        return view('userview.home');
        // return view('userview.home', [
        //     'categories' => Category::whereHas('contents', function ($query) {
        //         $query->whereNotNull('category_id');
        //     })->get(),
        // ]);
    }
    public function allContents()
    {
        return view('userview.allContents', [
            'contents' => Content::all(),
        ]);
        // return view('userview.allContents', [
        //     'contents' => Content::whereHas('documents', function ($query) {
        //         $query->whereNotNull('content_id');
        //     })->get(),
        // ]);
    }

    public function contentDocuments(Content $content)
    {
        return view('userview.documents', [
            'documents' => Document::where('content_id', $content->id)->get(),
        ]);
    }
    public function documentsLists()
    {
        return view('userview.documents', [
            'documents' => Document::all(),
        ]);
    }
    public function trainingsLists()
    {
        return view('userview.trainings', [
            'trainings' => Training::all(),
        ]);
        // return view('userview.documents', [
        //     'contents' => Content::where('type', 'trainings')->get(),
        // ]);
    }
    public function trainingsDetails($content)
    {


        return view('userview.test', [
            'trainings' => Training::where('content_id', $content)->get(),
            'content' => Content::where('id', $content)->first(),


        ]);
    }
}
