<?php

namespace App\Models;

use \Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;
    use Sluggable;

    /**
        * Return the sluggable configuration array for this model.
        *
        * @return array
        */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    protected $guarded =[];
    public function documents()
    {
        return $this->hasMany(Document::class);
    }
    public function subFolders()
    {
        return $this->hasMany(SubFolder::class);
    }
    public function trainings()
    {
        return $this->hasMany(Training::class);
    }

}
