<?php

namespace App\Models;

use \Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use HasFactory,Sluggable;
    protected $guarded = [];

    /**
      * Return the sluggable configuration array for this model.
      *
      * @return array
      */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'file_name'
            ]
        ];
    }
    public function subFolders()
    {
        return $this->belongsTo(SubFolder::class, 'sub_folder_id');
    }
    public function Content()
    {
        return $this->belongsTo(Content::class);
    }

}
