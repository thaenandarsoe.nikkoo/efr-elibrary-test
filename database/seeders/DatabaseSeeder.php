<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Content;
use App\Models\Document;
use App\Models\SubFolder;
use App\Models\Training;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::truncate();
        \App\Models\User::factory(5)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // Content::factory(5)->create();
        // Content::factory(5)->create();
        Content::factory()->create([
            'id'=>1,
            'name'=>'EFR Group Data',
            'slug'=>'efr-group-data',
            'type'=>'documents'
        ]);
        Content::factory()->create([
            'id'=>2,
            'name'=>'Laws & Regulations',
            'slug'=>'laws-and-regulations',
            'type'=>'documents'
        ]);
        Content::factory()->create([
              'id'=>3,
            'name'=>'HR Policy & Announcement',
            'slug'=>'hr-policy-and-announcement',
               'type'=>'documents',
        ]);
        Content::factory()->create([
              'id'=>4,
            'name'=>'Incoporation Data',
            'slug'=>'incoporation-data',
               'type'=>'documents',
        ]);
        Content::factory()->create([
              'id'=>5,
            'name'=>'Chat GPT Trainings',
            'slug'=>'chat-gpt-trainings',
               'type'=>'trainings',
        ]);
        Content::factory()->create([
              'id'=>6,
            'name'=>'Air Freight',
            'slug'=>'air-freight',
               'type'=>'trainings',
        ]);
        Content::factory()->create([
              'id'=>7,
            'name'=>'Excel',
            'slug'=>'excel',
               'type'=>'trainings',
        ]);
        Content::factory(2)->create();

        Document::factory()->create([
            'title'=>'Market Data',
            'content_id'=>'1',
        ]);
        Document::factory()->create([
            'title'=>'Group Data',
            'content_id'=>'1',
        ]);
        Document::factory()->create([
            'title'=>'Business in a box',
            'content_id'=>'3',
        ]);
        Document::factory()->create([
            'title'=>'Current Forms and format',
            'content_id'=>'3',
        ]);
        Document::factory()->create([
            'title'=>'Presentation and Letterhead',
            'content_id'=>'3',
        ]);
        Document::factory()->create([
            'title'=>'Legal Documents',
            'content_id'=>'2',
        ]);
        Document::factory(3)->create();

        // Training::factory()->create([
        //     'file_name' => 'How to draw logos on chat GPT',
        //     'content_id' => 5,
        //     'sub_folder_id' => 2,

        // ]);
        // Training::factory()->create([
        //     'file_name' => 'Lesson1',
        //     'content_id' => 6,
        //     'sub_folder_id' => null,

        // ]);
        // Training::factory()->create([
        //     'file_name' => 'Writing Emails Chat GPT',
        //      'content_id' => 5,
        //     'sub_folder_id' => 2,

        // ]);
        SubFolder::factory()->create([
            'name' => 'Chat GPT Course1',
            'content_id' => 5,
            'slug'=>"chat-gpt-course1"
        ]);
        SubFolder::factory()->create([
            'name' => 'Chat GPT Course2',
            'content_id' => 5,
            'slug'=>"chat-gpt-course2"
        ]);
        SubFolder::factory()->create([
            'name' => 'Basic',
            'content_id' => 7,
            'slug'=>"excel-basic"
        ]);
        SubFolder::factory()->create([
            'name' => 'Advance',
            'content_id' => 7,
            'slug'=>"excel-advance"
        ]);
    }
}
