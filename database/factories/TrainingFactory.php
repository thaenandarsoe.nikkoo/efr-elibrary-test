<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Training>
 */
class TrainingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'file_name' => fake()->title(),
            'slug' => fake()->slug(),

            'file' => fake()->filePath(),
            'description' => fake()->sentence(),
            'sub_folder_id' => 1,
            'uploaded_by'=>fake()->name()
        ];
    }
}
