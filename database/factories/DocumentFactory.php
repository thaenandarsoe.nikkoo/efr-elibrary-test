<?php

namespace Database\Factories;

use App\Models\Content;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Document>
 */
class DocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        //         $table->string('title');
        // $table->string('slug')->unique();
        // $table->string('content');
        // $table->string('description')->nullable();
        // $table->text('link');
        // $table->string('uploaded_by');

        return [
            'title' => fake()->name(),
            'slug' => fake()->slug(),
            'content_id' => Content::factory()->create(),
            'description'=>fake()->sentence(),
            'link'=>'https://efrgroupmyanmar-my.sharepoint.com/:f:/g/personal/designer_efrgroupmm_com/Er8Mk7zvlnVFiVnzqM9QO7oBTuJACKcYVF7rtkx3UJGvdQ?e=s2GQt0',
            'uploaded_by'=>fake()->name(),
        ];
    }
}
